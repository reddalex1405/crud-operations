package com.davidreyes.rest.facturas;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apitechu/v2")
public class FacturasController {
    @Autowired FacturasRepository facturasRepository;


    @GetMapping("/facturas")
    public Iterable<FacturaModel> getFacturas(@RequestParam(defaultValue = "0") double hasta) {
        if (hasta == 0) {
            return facturasRepository.findAll();
        } else {
            return facturasRepository.findByImporte(hasta);
        }

    }
    @GetMapping("/facturas/fecha")
    public Iterable<FacturaModel> getFacturasFecha(@RequestParam(defaultValue = "") String fecha) {
        if (fecha == "") {
            return facturasRepository.findAll();
        } else {
            return facturasRepository.findByFecha(fecha);
        }

    }






}
