package com.davidreyes.rest.productos;

import org.springframework.data.repository.CrudRepository;

public interface ProductosRepository extends CrudRepository<ProductoModel, Integer> {


}