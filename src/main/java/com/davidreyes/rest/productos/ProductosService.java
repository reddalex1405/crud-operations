package com.davidreyes.rest.productos;

import com.davidreyes.rest.facturas.FacturasRepository;
import com.davidreyes.rest.facturas.FacturaModel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class ProductosService {

    @Autowired
    ProductosRepository productosRepository;

    @Autowired
    FacturasRepository facturasRepository;

    @Transactional(rollbackOn = {Exception.class})
    public void crearFactura(String cliente, int idProducto) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = sdf.format(new Date());
        Optional<ProductoModel> optProd = productosRepository.findById(idProducto);
        if(optProd.isPresent()) {
            ProductoModel prod = optProd.get();
            FacturaModel fm = new FacturaModel();
            fm.setCliente(cliente);
            fm.setIdProducto(idProducto);
            fm.setFecha(fecha);
            fm.setImporte(prod.getPrecio());
            facturasRepository.save(fm);

            if((prod.getStock()-1)<0) throw new Exception("Stock < 0");
            prod.setStock(prod.getStock()-1);
            productosRepository.save(prod);
        }
    }
}
